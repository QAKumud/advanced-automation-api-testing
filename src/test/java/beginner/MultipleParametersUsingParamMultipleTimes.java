package beginner;

import io.restassured.RestAssured;

public class MultipleParametersUsingParamMultipleTimes {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .param("category_id","12")
                .param("name","")
                .param("page","1")
                .param("page_size","10")
                .get("/products/list/")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
