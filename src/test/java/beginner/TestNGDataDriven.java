package beginner;

import io.restassured.RestAssured;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;

public class TestNGDataDriven {

    @Test(dataProvider = "files")
    void testInvalidTokenRequest(String JSONFile) {
        File jsonFile = new File(JSONFile);
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";

        RestAssured
                .given()
                .contentType("application/json")
                .body(jsonFile)
                .post("/users/jwt/token/")
                .then()
                .assertThat()
                .statusCode(400);

    }

    @DataProvider(name="files")
    public Object[][] getFiles() {
        return new Object[][] {
                {"src/test/java/beginner/JSONFile/data1.json"},
                {"src/test/java/beginner/JSONFile/data2.json"},
                {"src/test/java/beginner/JSONFile/data3.json"}
        };
    };



    //        Object[][] filesData = new Object[1][2];
//        filesData[0][0] ="src/test/java/beginner/JSONFile/data1.json";
//        filesData[0][1]= "src/test/java/beginner/JSONFile/data2.json";
//        return  filesData;


}
