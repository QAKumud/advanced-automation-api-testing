package beginner;

import io.restassured.RestAssured;

import java.util.HashMap;
import java.util.Map;

public class MultipleParametersUsingParams {
    public static void main(String[] args) {
        // Request Parameters - Passing Multiple Parameters
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        Map<String, String> requestParameters = new HashMap<String, String>();
        requestParameters.put("category_id", "1");
        requestParameters.put("name", "");
        requestParameters.put("page","1");
        requestParameters.put("page_size","10");
        RestAssured
                .given()
                .params(requestParameters)
                .get("/products/list/")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
