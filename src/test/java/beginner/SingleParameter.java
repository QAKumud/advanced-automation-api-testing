package beginner;

import io.restassured.RestAssured;

public class SingleParameter {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .param("category_id",17)
                .get("/products/list")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
