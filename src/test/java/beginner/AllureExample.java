package beginner;


import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.TmsLink;
import io.restassured.RestAssured;
import org.testng.annotations.Test;

@Epic("Allure Example")
public class AllureExample {

    @TmsLink("https://tms.com/testcase/11")
    @Description("Check for valid status code - 200")
    @Test
    void verifyStatusCode() {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .get("/products/category/")
                .then()
                .assertThat()
                .statusCode(200);
    }
}
