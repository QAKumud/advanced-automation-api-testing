package beginner;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.util.List;

public class JSONParseExample {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        Response response = RestAssured
                                .given()
                                .log().all()
                                .param("category_id","15")
                                .param("name","")
                                .param("page","1")
                                .param("page_size","10")
                                .get("/products/list/");
        JsonPath jsonPath = response.jsonPath();
        int count = jsonPath.getInt("count");
        System.out.println(count);

        List<String> productNameList = jsonPath.getList("results.name");
        System.out.println(productNameList);
        assert count == productNameList.size();

        RestAssured.given().auth().preemptive().basic("username", "password");
        RestAssured.given().auth().digest("username","password");
        RestAssured.given().auth().oauth2("token");
        RestAssured.given().contentType(ContentType.JSON).auth().oauth("consumerKey","consumerSecret","accessToken","tokenSecret");
    }
}
