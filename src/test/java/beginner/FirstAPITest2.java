package beginner;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class FirstAPITest2 {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        Response response = RestAssured
                .given()
                .get("/products/category/");
        assert (response.statusCode() == 201);
    }
}



