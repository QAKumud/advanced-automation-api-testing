package beginner;
import io.restassured.RestAssured;

import static org.hamcrest.Matchers.*;

public class UsingMatchers {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .log().all()
                .param("category_id","15")
                .param("name","")
                .param("page","1")
                .param("page_size","10")
                .get("/products/list/")
                .then()
                .assertThat()
                .statusCode(200)
                .body("count",equalTo(3))
                .body("results",notNullValue())
                .body("results[0].id",equalTo(27))
                .body("next",nullValue());

    }
}
