package beginner;

import io.restassured.RestAssured;

public class SingleHeader {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .log().all()
                .header("Authorization", "Bearer"+ " some_token")
                .get("/products/category/")
                .prettyPrint();
    }
}
