package beginner;

import io.restassured.RestAssured;

public class RequestBody {
    public static void main(String[] args) {
        // Example 1
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .log().all()
                .body("some_body")
                .get("/products/category/")
                .prettyPrint();

        // Example 2 - Sending JSON body
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .contentType("application/json")
                .log().all()
                .body("{\n" +
                        "                            \"email\": \"test1@user.com\",\n" +
                        "                            \"password\": \"test1@user\"\n" +
                        "                        }")
                .post("/users/jwt/token/")
                .prettyPrint();
    }
}
