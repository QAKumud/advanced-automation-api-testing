package beginner;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;

import java.util.HashMap;
import java.util.Map;

public class MultipleHeaderUsingHeaders {
    public static void main(String[] args) {
        // Request Parameters - Passing Multiple Parameters
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        Map<String, String> headersList = new HashMap<String, String>();
        headersList.put("Authorization", "some_token");
        headersList.put("Content Type", "application/json");
        headersList.put("accept","application/xml");
        headersList.put("header 4", "some_header");
        RestAssured
                .given()
                .log().all()
                .headers(headersList)
                .get("/products/list/")
                .then()
                .assertThat()
                .statusCode(400);
    }

}
