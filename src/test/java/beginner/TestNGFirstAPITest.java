package beginner;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class TestNGFirstAPITest {

    @Test
    public static void verifyStatusCode() {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .get("/products/category/")
                .then()
                .assertThat()
                .statusCode(200);
    }

}
