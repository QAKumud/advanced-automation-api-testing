package beginner;

import io.restassured.RestAssured;

public class RequestBodyAsString {
    public static void main(String[] args) {
        // Example 2 - Sending JSON body as String
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        String requestBody = " {\n" +
                "                            \"email\": \"test1@user.com\",\n" +
                "                            \"password\": \"test1@user\"\n" +
                "                }";
        RestAssured
                .given()
                .contentType("application/json")
                .log().all()
                .body(requestBody)
                .post("/users/jwt/token/")
                .prettyPrint();
    }
}
