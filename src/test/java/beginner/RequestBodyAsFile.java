package beginner;

import io.restassured.RestAssured;

import java.io.File;

public class RequestBodyAsFile {
    public static void main(String[] args) {
        File jsonFile = new File("src/test/java/beginner/JSONFile/loginData.json");
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";

        RestAssured
                .given()
                .contentType("application/json")
                .body(jsonFile)
                .post("/users/jwt/token/")
                .prettyPrint();
    }
}
