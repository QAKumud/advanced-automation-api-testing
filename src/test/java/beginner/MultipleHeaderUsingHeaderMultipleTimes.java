package beginner;

import io.restassured.RestAssured;

public class MultipleHeaderUsingHeaderMultipleTimes {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .log().all()
                .header("Authorization", "Bearer"+ " some_token")
                .header("Content Type","application/json")
                .header("accept","application/xml")
                .header("header 3", "some_header")
                .get("/products/category/")
                .prettyPrint();
    }
}
