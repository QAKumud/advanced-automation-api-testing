package beginner;

import io.restassured.RestAssured;

public class FirstAPITest1 {
    public static void main(String[] args) {
        RestAssured.baseURI ="https://test.api.testamplify.io/api/v1";
        RestAssured
                .given()
                .get("/products/category/")
                .then()
                .assertThat()
                .statusCode(200);
    }
}



